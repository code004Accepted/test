命令：
1. `npm start`：开始测试
2. `npm run packx64`：包装64位应用程序，存放于`/out/`内
3. `npm run packia32`：包装32位应用程序，存放于`/out/`内
4. `npm run packarm64`：包装ARM64应用程序，存放于`/out/`内

图标：icon.ico，存放于根目录下

Going To Do:
- [x] POST DATA TO SERVER：fixed by chuangzhi👍
- [x] 代刷页面：已部署于 http://upnt70.coding-pages.com/
- [x] PHP重定向到代刷页面
- [x] 加入二维码登录
- [x] 两种登录方式互相通信
- [x] 字体、字号修改
- [ ] ~~去除登录界面的加载动画~~ **wontfix**

项目主线进度：

- [x] 代刷平台：完成100% 可选：将iframe高度适当调小
- [x] Electron：完成100%，可能后续会有更多优化
- [x] PHP远程调用接口
- [ ] E语言主程序：完成0%，已分配给chuangzhi
- [ ] 其它任务：待定